
const mv = require('mv');

const fs = require('fs');

const config = require('../config.js');

//------------------------------------------------------------------

class File {

    constructor(config) {
        this.uploadPath = config.uploadPath;
    }

    dateCodedPath() {
        // gen a utc data as string (yyyy/mm/dd)
        return new Date().toJSON().slice(0,10).replace(/-/g, '/');
    }

    timeCodedFilename() {
        // get time (osx workaround)
        let date = new Date();
        let hh = date.getHours();
        let mm = date.getMinutes();
        let ss = date.getSeconds();
        // gen a utc time as string (hh-mm-ss)
        let utc_time = hh + '-' + mm + '-' + ss;
        // gen random part
        let random_part = Math.ceil((Math.random() * (9999-1000))) + 1000;
        // return final filename
        return utc_time + '_' + random_part;

    }


    uploadFile(uploadFileObject) {
        return new Promise((resolve, reject) => {

            // check if file is present
            if (!uploadFileObject) return reject('No file given');

            // create some names
            let uploadFilePath =  this.uploadPath;
            let newFileExtension = uploadFileObject.mimetype.split('/')[1];
            let newFileName = this.timeCodedFilename() + '.' + newFileExtension;
            let newFilePath = this.dateCodedPath() + '/' + newFileName;
            let newFileFullname = uploadFilePath + '/' + newFilePath;
            let tempFile = uploadFilePath + '/' + newFileName;

            // move file to uploads folder
            uploadFileObject.mv(tempFile, function (err) {
                if (err) return reject(err);

                // move file to final folder
                mv(tempFile, newFileFullname, {mkdirp: true}, function (err) {
                    if (err) return reject(err);

                    console.log('uploadFileObject: ', uploadFileObject);
                    console.log('newFileFullname: ', newFileFullname);

                    resolve(newFileFullname);

                });

            });

        });
    }


    deleteFile(filename) {

        // TODO delete folder recursively

        // promisify fs.unlink
        return new Promise((resolve, reject) => {
            fs.unlink(filename, (err) => {
                if (err)  reject(err);
                resolve();
            });
        });
    }



}

module.exports = new File(config.fileUploadConfig);
