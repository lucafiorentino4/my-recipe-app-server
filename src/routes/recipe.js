let fileUpload = require("../lib/File");
let db = require('../lib/Database.js');

// INSERT ROUTES ----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
// function to insert data in DB for Occasion
function addedOccasion(addOccasion, recipeID){
    let OccasionValues = '';

    if (addOccasion.length === 0) return;

    addOccasion.forEach((occasion) => {
        OccasionValues +=  ' ('+recipeID+', '+occasion+'), ';
    });

    OccasionValues = OccasionValues.slice(0, -2);

    let sql = 'INSERT INTO recipe_occasion (recipe_id, occasion_id) VALUES ' + OccasionValues + ';';

    db.query(sql).then(() => {
       // res.send();
    })
}

// route: /api/likeRecipe
exports.likeRecipe = function(req, res) {

    let recipe_id = req.body.recipe_id;
    let user_id = req.body.user_id;
    let like = req.body.like_recipe;

    let sql = (like) ? 'INSERT INTO likes (recipe_id, user_id) VALUES (?, ?)' : 'DELETE FROM likes WHERE recipe_id = ? AND user_id = ?';

    db.query(sql, [recipe_id, user_id]).then(() => {
        res.send();
    });

};

// route: /api/addNewRecipe
exports.addNewRecipe = function(req, res) {

    let addOccasion = req.body.occasion;
    let uploadedFileObject = req.files.recipeImage;

// SELECTED TIME UNIT ---------------------------------------------
    let preparationTU = parseInt( req.body.preparation_timeUnit_id);
    let cookingTU = parseInt( req.body.cooking_timeUnit_id);
    let waitTU = parseInt( req.body.waiting_timeUnit_id);
//-----------------------------------------------------------------

// CONVERT TIME IN HOURS
// cnvt = converted
    let cnvtPreparationTime = (preparationTU === 2) ? (parseInt( req.body.preparationTime) * 60) :  req.body.preparationTime;
    let cnvtCookingTime = (cookingTU === 2) ? (parseInt( req.body.cookingTime) * 60) :  req.body.cookingTime;
    let cnvtWaitingTime = (waitTU === 2) ? (parseInt( req.body.waitingTime) * 60) :  req.body.waitingTime;


    fileUpload.uploadFile(uploadedFileObject).then((result) => {
        //prepare image array
        let imageArray = {
            name: uploadedFileObject.name,
            image_url: result,
            size: uploadedFileObject.size
        };

        // add image to db
        db.query('INSERT INTO image SET ?', imageArray).then((result) => {
            let image_id = result.insertId;
            //prepare recipe array
            let recipe_array = {
                user_id: req.body.user_id,
                name: req.body.name,
                difficulty_id: req.body.difficulty_id,
                category: req.body.category,
                recipetype: req.body.recipetype,
                kitchenland: req.body.kitchenland,
                portions: req.body.portions,
                portionUnit: req.body.portionUnit,
                preparationTime: cnvtPreparationTime,
                cookingTime: cnvtCookingTime,
                waitingTime: cnvtWaitingTime,
                description: req.body.description,
                // convert public from string to boolean
                public: JSON.parse(req.body.public),
                image_id: image_id
            };

            // add recipe to db
            let sql = 'INSERT INTO recipe SET ?';
            db.query(sql, recipe_array).then((result) => {

                let recipeID = result.insertId;

                // prepare data for adding in database
                // convert from string to array
                let convertOccasionInArray = addOccasion.split(',');
                // add to database
                addedOccasion(convertOccasionInArray, recipeID);

                res.send();
            });
        });
    });
};

// route: /api/updateRecipe
exports.updateRecipe =  async function(req, res) {

    let uploadedUpdatedFileObject = (!req.files || Object.keys(req.files).length === 0) ? null : req.files.recipeImage;

    let recipe_id = req.body.recipe_id;
    let occasion = JSON.parse(req.body.occasion);



// SELECTED TIME UNIT ---------------------------------------------
    let preparationTU = parseInt(req.body.preparation_timeUnit_id);
    let cookingTU = parseInt( req.body.cooking_timeUnit_id);
    let waitTU = parseInt( req.body.waiting_timeUnit_id);
//-----------------------------------------------------------------

// CONVERT TIME IN HOURS
// cnvt = converted
    let cnvtPreparationTime = (preparationTU === 2) ? (parseInt( req.body.preparationTime) * 60) :  req.body.preparationTime;
    let cnvtCookingTime = (cookingTU === 2) ? (parseInt( req.body.cookingTime) * 60) :  req.body.cookingTime;
    let cnvtWaitingTime = (waitTU === 2) ? (parseInt( req.body.waitingTime) * 60) :  req.body.waitingTime;

    //-----------------------------------------------

    let selectImage =   `
            SELECT r.image_id, i.image_url AS image_url
            FROM recipe AS r
            LEFT JOIN image AS i on r.image_id = i.id
            WHERE r.id = ?;
         `;

    let sql_update = `
UPDATE recipe SET user_id = ?, name = ?, difficulty_id = ?, category = ?, recipetype = ?, kitchenland = ?,
portions = ?, portionUnit = ?, preparationTime = ?, cookingTime = ?, waitingTime = ?,
description = ?, public = ?, image_id = ? WHERE id = ?
`;


    // get img from db
    let result = await db.query(selectImage, [recipe_id]);
    let image_id = result[0].image_id;
    let image_url = result[0].image_url;

    if (uploadedUpdatedFileObject) {
        // delete file
        result = await fileUpload.deleteFile(image_url);
        // delete img from db
        result = await db.query(`DELETE FROM image WHERE id = ?`, [image_id]);
        // upload new file
        let newFilename = await fileUpload.uploadFile(uploadedUpdatedFileObject);
        let imageArray = {
            name: uploadedUpdatedFileObject.name,
            image_url: newFilename,
            size: uploadedUpdatedFileObject.size
        };
        result = await db.query('INSERT INTO image SET ?', imageArray);
        image_id = result.insertId;
    }

    //prepare recipe array
    let r_a = {
        user_id: req.body.user_id,
        name: req.body.name,
        difficulty_id: req.body.difficulty_id,
        category: req.body.category,
        recipetype: req.body.recipetype,
        kitchenland: req.body.kitchenland,
        portions: req.body.portions,
        portionUnit: req.body.portionUnit,
        preparationTime: cnvtPreparationTime,
        cookingTime: cnvtCookingTime,
        waitingTime: cnvtWaitingTime,
        description: req.body.description,
        // convert public from string to boolean
        public: JSON.parse(req.body.public),
        image_id: image_id,
        id: recipe_id};
    // update recipe
    result = await db.query(sql_update, [r_a.user_id, r_a.name, r_a.difficulty_id,r_a.category, r_a.recipetype,
        r_a.kitchenland, r_a.portions, r_a.portionUnit, r_a.preparationTime,
        r_a.cookingTime, r_a.waitingTime, r_a.description, r_a.public, r_a.image_id, r_a.id]);


    // delete old occasion
    result = await db.query('DELETE FROM recipe_occasion WHERE recipe_id = ?', recipe_id);
    // update occasion (add as new ones)
    let sql_occ = 'INSERT INTO recipe_occasion (recipe_id, occasion_id) VALUES ?';
    let occasion_insert_array = occasion.map(item => [recipe_id, item]);
    result = await db.query(sql_occ, [occasion_insert_array]);

    res.send();
};

// DELETE ROUTES

// route: /api/deleteRecipe
exports.deleteRecipe = function(req, res) {

    let recipe_id = req.body.recipe_id;

    let sql = `SELECT r.image_id, i.image_url
               FROM recipe AS r
               LEFT JOIN image AS i on r.image_id = i.id
               WHERE r.id = ?;`;

    // get image_id and image_filename
    db.query(sql, [recipe_id]).then((row) => {
        console.log(row[0]);

        let image_id = row[0].image_id;
        let image_url = row[0].image_url;

        // remove file from folder (todo: remove folder)
        fileUpload.deleteFile(image_url).then(() => {
            // remove image form db
            db.query(`DELETE FROM image WHERE id = ?`, [image_id]).then(() => {
                // remove all recipe occasion form db
                db.query(`DELETE FROM recipe_occasion WHERE recipe_id = ?`, [recipe_id]).then(() => {
                    // remove all recipe occasion form db
                    db.query(`DELETE FROM likes WHERE recipe_id = ? `, [recipe_id]).then(() => {

                        db.query(`DELETE FROM recipe WHERE id = ?`, [recipe_id]).then(() => {
                            res.send();
                        });
                    })
                });
            });
        });
    });
};

// GET ROUTES ----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------


// route: /api/getRecipe
exports.getRecipe = function(req, res) {
    let recipe_id = req.body.recipe_id;
    let user_id = req.body.user_id;

    let sql_recipeData = `SELECT r.*, i.image_url, i.size, c.name  AS cat_name, u.firstname AS user_firstname, u.lastname AS user_lastname, rt.name AS rt_name, kl.name AS kl_name, dff.difficulty_text, !ISNULL( l.id ) AS like_recipe,
                          (SELECT COUNT(id) FROM likes AS lk WHERE lk.recipe_id = r.id) AS 'like_count'                   
                          FROM recipe as r
                          LEFT JOIN user AS u ON u.id = r.user_id 
                          LEFT JOIN image AS i ON i.id = r.image_id
                          LEFT JOIN category AS c ON c.id = r.category
                          LEFT JOIN recipetype AS rt ON rt.id = r.recipetype
                          LEFT JOIN kitchenland AS kl ON kl.id = r.kitchenland
                          LEFT JOIN difficulty AS dff ON dff.id = r.difficulty_id
                          LEFT JOIN likes AS l ON r.id = l.recipe_id AND l.user_id = ?
                          WHERE r.id = ?;`;

    let sql_occasion = `SELECT rocc.occasion_id AS id, occ.name AS occasion_name
                       FROM recipe_occasion AS rocc
                       LEFT JOIN occasion AS occ ON occ.id = rocc.occasion_id
                       WHERE recipe_id = ?;`;

    db.query(sql_recipeData, [user_id, recipe_id]).then((result_1) => {
        let recipeData = result_1[0];

        db.query(sql_occasion, recipe_id).then((result_2) => {
            recipeData['occasionMap'] = result_2.map(item => item.id );
            recipeData['occasion'] = result_2;


            res.send(recipeData)
        });
    });
};

// route: /api/getRecipeFromOffline
exports.getRecipeFromOffline = function(req, res) {
    let recipe_id = req.body.recipe_id;

    let sql_recipeData = `SELECT r.*, i.image_url, i.size, c.name  AS cat_name, u.firstname AS user_firstname, u.lastname AS user_lastname, rt.name AS rt_name, kl.name AS kl_name, dff.difficulty_text,
                          (SELECT COUNT(id) FROM likes AS lk WHERE lk.recipe_id = r.id) AS 'like_count'                   
                          FROM recipe as r
                          LEFT JOIN user AS u ON u.id = r.user_id 
                          LEFT JOIN image AS i ON i.id = r.image_id
                          LEFT JOIN category AS c ON c.id = r.category
                          LEFT JOIN recipetype AS rt ON rt.id = r.recipetype
                          LEFT JOIN kitchenland AS kl ON kl.id = r.kitchenland
                          LEFT JOIN difficulty AS dff ON dff.id = r.difficulty_id
                          WHERE r.id = ?;`;

    let sql_occasion = `SELECT rocc.occasion_id AS id, occ.name AS occasion_name
                       FROM recipe_occasion AS rocc
                       LEFT JOIN occasion AS occ ON occ.id = rocc.occasion_id
                       WHERE recipe_id = ?;`;

    db.query(sql_recipeData, [recipe_id]).then((result_1) => {
        let recipeData = result_1[0];

        db.query(sql_occasion, recipe_id).then((result_2) => {
            recipeData['occasionMap'] = result_2.map(item => item.id );
            recipeData['occasion'] = result_2;


            res.send(recipeData)
        });
    });
};

// route: /api/getAllPublicRecipes
exports.getAllPublicRecipes = function(req, res) {
    let user_id = req.body.user_id;

    let sql = `SELECT r.*, u.firstname AS user_firstname, u.lastname AS user_lastname, i.image_url, i.size, dff.difficulty_text,
               (SELECT COUNT(id) FROM likes AS lk WHERE lk.recipe_id = r.id) AS 'like_count'
               FROM recipe as r 
               LEFT JOIN user AS u ON u.id = r.user_id 
               LEFT JOIN image AS i ON i.id = r.image_id
               LEFT JOIN difficulty AS dff ON dff.id = r.difficulty_id
               WHERE public = 1
               ORDER BY r.id;`;

    db.query(sql, user_id).then((rows) => {
        res.send(rows);
    });
};

// route: /api/getAllPublicRecipesWithSearch
exports.getAllPublicRecipesWithSearch = function(req, res) {
    let searchValue = req.body.searchValue;
    let user_id = req.body.user_id;
    let sV = '%' + searchValue + '%';

    let sql = `SELECT r.*, u.firstname AS user_firstname, u.lastname AS user_lastname, i.image_url, i.size , kl.name AS kt_name, kl.countryCode,ct.name AS ct_name, rt.name AS rt_name, dff.difficulty_text,
               (SELECT COUNT(id) FROM likes AS lk WHERE lk.recipe_id = r.id) AS 'like_count'
               FROM recipe as r
               LEFT JOIN user AS u ON u.id = r.user_id
               LEFT JOIN image AS i ON i.id = r.image_id
               LEFT JOIN kitchenland AS kl ON kl.id = r.kitchenland
               LEFT JOIN category AS ct ON ct.id = r.category
               LEFT JOIN recipetype AS rt ON rt.id = r.recipetype
               LEFT JOIN difficulty AS dff ON dff.id = r.difficulty_id
               WHERE r.public = 1 AND 
                    (r.name LIKE ? OR r.description LIKE ? OR
                     u.firstname LIKE ? OR u.lastname LIKE ? OR 
                     kl.name LIKE ? OR ct.name LIKE ? OR rt.name LIKE ?) ORDER BY r.id;`;

    db.query(sql, [user_id, sV,sV,sV,sV,sV,sV,sV]).then((rows) => {
        if (rows.length === 0){
            res.send(false);
        }else{
            res.send(rows);}
    });
};

// route: /api/getTenPublicRecipes
exports.getTenPublicRecipes = function(req, res) {

    let sql = `SELECT r.*, u.firstname AS user_firstname, u.lastname AS user_lastname, i.image_url, i.size, dff.difficulty_text,
               (SELECT COUNT(id) FROM likes AS lk WHERE lk.recipe_id = r.id) AS 'like_count'               
               FROM recipe as r
               LEFT JOIN user AS u ON u.id = r.user_id
               LEFT JOIN image AS i ON i.id = r.image_id
               LEFT JOIN difficulty AS dff ON dff.id = r.difficulty_id
               WHERE public = 1 ORDER BY r.id
               LIMIT 10;`;

    db.query(sql).then((rows) => {
        res.send(rows);
    });
};

// route: /api/getAllRecipesForUser
exports.getAllRecipesForUser = function(req, res) {
    let user_id = req.body.user_id;
    db.query(`
        SELECT r.*, i.image_url, i.size, d.difficulty_text,
        (SELECT COUNT(id) FROM likes AS lk WHERE lk.recipe_id = r.id) AS 'like_count'
        FROM recipe as r
        LEFT JOIN image AS i ON i.id = r.image_id
        LEFT JOIN difficulty AS d on r.difficulty_id = d.id
        WHERE user_id = ? 
        ORDER BY r.id;`, [user_id]).then((rows) => {
        res.send(rows);
    });
};

// route: /api/getFirstFourRecipesForUser
exports.getFirstFourRecipesForUser = function(req, res) {
    let user_id = req.body.user_id;
    let sql = `SELECT r.*, i.image_url, i.size, u.firstname AS user_firstname, u.lastname AS user_lastname, d.difficulty_text,
        (SELECT COUNT(id) FROM likes AS lk WHERE lk.recipe_id = r.id) AS 'like_count'
        FROM recipe as r
        LEFT JOIN image AS i ON i.id = r.image_id
        LEFT JOIN user AS u ON u.id = r.user_id
        LEFT JOIN difficulty AS d on r.difficulty_id = d.id
        WHERE user_id = ? 
        ORDER BY r.id
        LIMIT 4;`;

    db.query(sql, [user_id]).then((rows) => {
        res.send(rows);
    });
};

// route: /api/getDataFromDB
exports.getDataFromDB = function(req, res) {
    let sql_getCategories = `SELECT * FROM category WHERE 1`;
    let sql_getRecipeType = `SELECT * FROM recipetype WHERE 1`;
    let sql_getKitchenLands = `SELECT * FROM kitchenland WHERE 1`;
    let sql_getOccasions = `SELECT * FROM occasion WHERE 1`;


    db.query(sql_getCategories).then((categories) => {
        let allCategoriesData = categories;
        db.query(sql_getRecipeType).then((recipeType) => {
            let allRecipeTypes = recipeType;
            db.query(sql_getKitchenLands).then((kitchenLands) => {
                let allKitchenLands = kitchenLands;
                db.query(sql_getOccasions).then((allOccasions) => {
                    res.send({allCategoriesData: allCategoriesData, allRecipeTypes: allRecipeTypes, allKitchenLands: allKitchenLands, allOccasions: allOccasions})
                });
            });
        });
    });
};

