const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config.js');

let db = require('../lib/Database.js');


// route: /api/registerUser
exports.registerUser = function(req, res) {

    let user = req.body.user;

    // check if user exsists
    db.query('SELECT * FROM user WHERE email = ?', [user.email]).then(result => {
        if (result.length === 0) {

            // hash password
            let salt = 10;
            bcrypt.hash(user.password, salt).then(hashedPass => {
                let user_array = [user.firstname, user.lastname, user.email, user.nickname, hashedPass];
                // insert in db
                db.query('INSERT INTO user (firstname, lastname, email, nickname, password) VALUES (?, ?, ?, ?, ?)', user_array).then(() => {
                    res.send('Success');
                });
            });

        } else {
            res.status(500).send('User already registerd')
        }
    });
};

// route: /api/loginUser
exports.loginUser = function(req, res) {
    let user = req.body.user;

    // check if user exsists
    db.query('SELECT * FROM user WHERE email = ?', [user.email]).then(result => {
        if (result.length === 0) {
            res.status(500).send('Username or password wrong');
        } else {
            let sqlResult = result[0];

            // get hashed password from db
            let hash = sqlResult.password;
            // check password hash
            bcrypt.compare(user.password, hash).then((bryptResult) => {
                if (bryptResult) {

                    // generate user data
                    const user = {
                        id: sqlResult.id,
                        firstname: sqlResult.firstname,
                        lastname: sqlResult.lastname,
                        email: sqlResult.email,
                        nickname: sqlResult.nickname,
                        role: sqlResult.role
                    };

                    // generate token
                    let token = jwt.sign(user, config.jsonwebtokenConfig.jwtSecret, config.jsonwebtokenConfig.jwtConfig);

                    res.send({user:user, token:token});
                } else {
                    res.status(500).send('Username or password wrong');
                }
            });

        }

    });
};
