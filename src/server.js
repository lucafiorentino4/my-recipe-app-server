// Imports
const checkJwt = require('express-jwt');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');

const app = express();

//------------------------------------------------------------------------------

const config = require('./config.js');

const user_routes = require('./routes/user.js');
const recipe_routes = require('./routes/recipe.js');

//------------------------------------------------------------------------------
// app settings

// support json-encoded bodies
app.use(bodyParser.json());

// eneable cors
app.use(cors());

// enable fileUpload
app.use(fileUpload());

// eneable static files
app.use('/' + config.fileUploadConfig.uploadPath, express.static(config.fileUploadConfig.uploadPath));

// Protection
app.use(checkJwt({secret: config.jsonwebtokenConfig.jwtSecret}).unless({path: ['/api/loginUser', '/api/registerUser', '/api/getTenPublicRecipes', '/api/getRecipeFromOffline']}));

//------------------------------------------------------------------------------
// Routes
// user-routes
app.post('/api/registerUser', user_routes.registerUser);
app.post('/api/loginUser', user_routes.loginUser);

//------------------------------------------------------------------------------
// recipe-routes
app.post('/api/likeRecipe', recipe_routes.likeRecipe);
app.post('/api/addNewRecipe', recipe_routes.addNewRecipe);
app.post('/api/updateRecipe', recipe_routes.updateRecipe);

// GET Recipes
app.post('/api/getRecipe', recipe_routes.getRecipe);
app.post('/api/getRecipeFromOffline', recipe_routes.getRecipeFromOffline);
app.post('/api/getAllPublicRecipes', recipe_routes.getAllPublicRecipes);
app.post('/api/getAllPublicRecipesWithSearch', recipe_routes.getAllPublicRecipesWithSearch);
app.post('/api/getTenPublicRecipes', recipe_routes.getTenPublicRecipes);
app.post('/api/getAllRecipesForUser', recipe_routes.getAllRecipesForUser);
app.post('/api/getFirstFourRecipesForUser', recipe_routes.getFirstFourRecipesForUser);

// GET Categories
app.post('/api/getDataFromDB', recipe_routes.getDataFromDB);

// DELETE Recipe
app.post('/api/deleteRecipe', recipe_routes.deleteRecipe);

//------------------------------------------------------------------------------
// start Server

app.set('port', config.expressConfig.port);

app.listen(app.get('port'), function() {
  console.log('Express Server listening on port ' + app.get('port'));
});
