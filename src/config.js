
module.exports = {

  'expressConfig': {
    port: 5000
  },

  'fileUploadConfig': {
    uploadPath: 'uploads'
  },

  'mysqlConfig': {
    host     : 'localhost',
    port     : 8889,
    user     : 'recipeapp_db',
    database : 'recipeapp_db',
    password : 'recipeapp_pw'
  },

  'jsonwebtokenConfig': {
    jwtSecret : 'geheim',

    jwtConfig : {
      algorithm: 'HS256',
      expiresIn: '24h',
      noTimestamp: false,

      header: {
        dies: 'das'
      }

    }
  }

};
