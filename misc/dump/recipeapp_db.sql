-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Creato il: Feb 02, 2020 alle 16:49
-- Versione del server: 5.7.24-log
-- Versione PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recipeapp_db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Vegan'),
(2, 'Vegetarian'),
(3, 'Non vegetarian'),
(5, 'Seafood');

-- --------------------------------------------------------

--
-- Struttura della tabella `difficulty`
--

CREATE TABLE `difficulty` (
  `id` int(11) NOT NULL,
  `difficulty_text` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `difficulty`
--

INSERT INTO `difficulty` (`id`, `difficulty_text`) VALUES
(1, 'Really easy'),
(2, 'Easy'),
(3, 'Middle'),
(4, 'Hard'),
(5, 'Very hard');

-- --------------------------------------------------------

--
-- Struttura della tabella `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `size` varchar(64) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `image`
--

INSERT INTO `image` (`id`, `name`, `image_url`, `size`, `created`) VALUES
(84, 'photo-1485451456034-3f9391c6f769.jpg', 'uploads/2020/02/02/11-17-26_3698.jpeg', '253341', '2020-02-02 10:17:26'),
(85, 'sebastian-coman-photography-mVwQrsnnbHg-unsplash.jpg', 'uploads/2020/02/02/11-20-53_9703.jpeg', '4225375', '2020-02-02 10:20:53'),
(87, 'photo-1494390248081-4e521a5940db.jpg', 'uploads/2020/02/02/11-26-23_7969.jpeg', '389997', '2020-02-02 10:26:23'),
(88, 'nerfee-mirandilla-KxcYYoJZehI-unsplash.jpg', 'uploads/2020/02/02/11-28-22_5713.jpeg', '4286723', '2020-02-02 10:28:22'),
(89, 'eaters-collective-12eHC6FxPyg-unsplash.jpg', 'uploads/2020/02/02/11-30-26_3733.jpeg', '1843706', '2020-02-02 10:30:26'),
(90, 'louis-hansel-cC0_UO1Obg4-unsplash.jpg', 'uploads/2020/02/02/11-33-2_6236.jpeg', '3738019', '2020-02-02 10:33:02'),
(91, 'photo-1518779578993-ec3579fee39f.jpg', 'uploads/2020/02/02/11-34-57_5565.jpeg', '727375', '2020-02-02 10:34:57'),
(92, 'chad-montano--GFCYhoRe48-unsplash.jpg', 'uploads/2020/02/02/11-39-30_1865.jpeg', '3667190', '2020-02-02 10:39:30'),
(93, 'brooke-lark-jUPOXXRNdcA-unsplash.jpg', 'uploads/2020/02/02/11-41-35_5641.jpeg', '5640422', '2020-02-02 10:41:35'),
(94, 'brooke-lark-of0pMsWApZE-unsplash.jpg', 'uploads/2020/02/02/12-20-7_7915.jpeg', '1868814', '2020-02-02 11:20:07'),
(95, 'photo-1483918793747-5adbf82956c4.jpg', 'uploads/2020/02/02/12-23-47_5963.jpeg', '488856', '2020-02-02 11:23:47'),
(96, 'ting-tian-al9eh9QkdPA-unsplash.jpg', 'uploads/2020/02/02/12-39-40_2983.jpeg', '14889627', '2020-02-02 11:39:40'),
(97, 'dilyara-garifullina-Lkb1g9ivC2c-unsplash.jpg', 'uploads/2020/02/02/12-42-18_7294.jpeg', '1539906', '2020-02-02 11:42:18'),
(98, 'jesse-hanley-FOvoZuWwrSk-unsplash.jpg', 'uploads/2020/02/02/12-43-47_6082.jpeg', '7062787', '2020-02-02 11:43:47'),
(99, 'eric-mcnew-xXzJ6mpcdwY-unsplash.jpg', 'uploads/2020/02/02/12-45-48_5780.jpeg', '5006962', '2020-02-02 11:45:48');

-- --------------------------------------------------------

--
-- Struttura della tabella `kitchenland`
--

CREATE TABLE `kitchenland` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `countryCode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `kitchenland`
--

INSERT INTO `kitchenland` (`id`, `name`, `countryCode`) VALUES
(1, 'Italy', 'it'),
(2, 'Afghanistan', 'af'),
(3, 'Aland Islands', 'ax'),
(4, 'Algeria', 'dz'),
(5, 'American Samoa', 'as'),
(6, 'Andorra', 'ad'),
(7, 'Angola', 'ao'),
(8, 'Anguilla', 'ai'),
(9, 'Antigua', 'ag'),
(10, 'Argentina', 'ar'),
(11, 'Armenia', 'am'),
(12, 'Aruba', 'aw'),
(13, 'Australia', 'au'),
(14, 'Austria', 'at'),
(15, 'Azerbaijan', 'az'),
(16, 'Bahamas', 'bs'),
(17, 'Bahrain', 'bh'),
(18, 'Bangladesh', 'bd'),
(19, 'Barbados', 'bb'),
(20, 'Belarus', 'by'),
(21, 'Belgium', 'be'),
(22, 'Belize', 'bz'),
(23, 'Benin', 'bj'),
(24, 'Bermuda', 'bm'),
(25, 'Bhutan', 'bt'),
(26, 'Bolivia', 'bo'),
(27, 'Bosnia', 'ba'),
(28, 'Botswana', 'bw'),
(29, 'Bouvet Island', 'bv'),
(30, 'Brazil', 'br'),
(31, 'British Virgin Islands', 'vg'),
(32, 'Brunei', 'bn'),
(33, 'Bulgaria', 'bg'),
(34, 'Burkina Faso', 'bf'),
(35, 'Burma', 'mm'),
(36, 'Burundi', 'bi'),
(37, 'Caicos Islands', 'tc'),
(38, 'Cambodia', 'kh'),
(39, 'Cameroon', 'cm'),
(40, 'Canada', 'ca'),
(41, 'Cape Verde', 'cv'),
(42, 'Cayman Islands', 'ky'),
(43, 'Central African Republic', 'cf'),
(44, 'Chad', 'td'),
(45, 'Chile', 'cl'),
(46, 'China', 'cn'),
(47, 'Christmas Island', 'cx'),
(48, 'Cocos Islands', 'cc'),
(49, 'Colombia', 'co'),
(50, 'Comoros', 'km'),
(51, 'Congo', 'cd'),
(52, 'Congo Brazzaville', 'cg'),
(53, 'Cook Islands', 'ck'),
(54, 'Costa Rica', 'cr'),
(55, 'Cote Divoire', 'ci'),
(56, 'Croatia', 'hr'),
(57, 'Cuba', 'cu'),
(58, 'Cyprus', 'cy'),
(59, 'Czech Republic', 'cz'),
(60, 'Denmark', 'dk'),
(61, 'Djibouti', 'dj'),
(62, 'Dominica', 'dm'),
(63, 'Dominican Republic', 'do'),
(64, 'Ecuador', 'ec'),
(65, 'Egypt', 'eg'),
(66, 'El Salvador', 'sv'),
(67, 'Equatorial Guinea', 'gq'),
(68, 'Eritrea', 'er'),
(69, 'Estonia', 'ee'),
(70, 'Ethiopia', 'et'),
(71, 'Europeanunion', 'eu'),
(72, 'Falkland Islands', 'fk'),
(73, 'Faroe Islands', 'fo'),
(74, 'Fiji', 'fj'),
(75, 'Finland', 'fi'),
(76, 'France', 'fr'),
(77, 'French Guiana', 'gf'),
(78, 'French Polynesia', 'pf'),
(79, 'French Territories', 'tf'),
(80, 'Gabon', 'ga'),
(81, 'Gambia', 'gm'),
(82, 'Georgia', 'ge'),
(83, 'Germany', 'de'),
(84, 'Ghana', 'gh'),
(85, 'Gibraltar', 'gi'),
(86, 'Greece', 'gr'),
(87, 'Greenland', 'gl'),
(88, 'Grenada', 'gd'),
(89, 'Guadeloupe', 'gp'),
(90, 'Guam', 'gu'),
(91, 'Guatemala', 'gt'),
(92, 'Guinea', 'gn'),
(93, 'Guinea-Bissau', 'gw'),
(94, 'Guyana', 'gy'),
(95, 'Haiti', 'ht'),
(96, 'Heard Island', 'hm'),
(97, 'Honduras', 'hn'),
(98, 'Hong Kong', 'hk'),
(99, 'Hungary', 'hu'),
(100, 'Iceland', 'is'),
(101, 'India', 'in'),
(102, 'Indian Ocean Territory', 'io'),
(103, 'Indonesia', 'id'),
(104, 'Iran', 'ir'),
(105, 'Iraq', 'iq'),
(106, 'Ireland', 'ie'),
(107, 'Israel', 'il'),
(108, 'Italy', 'it'),
(109, 'Jamaica', 'jm'),
(110, 'Jan Mayen', 'sj'),
(111, 'Japan', 'jp'),
(112, 'Jordan', 'jo'),
(113, 'Kazakhstan', 'kz'),
(114, 'Kenya', 'ke'),
(115, 'Kiribati', 'ki'),
(116, 'Kuwait', 'kw'),
(117, 'Kyrgyzstan', 'kg'),
(118, 'Laos', 'la'),
(119, 'Latvia', 'lv'),
(120, 'Lebanon', 'lb'),
(121, 'Lesotho', 'ls'),
(122, 'Liberia', 'lr'),
(123, 'Libya', 'ly'),
(124, 'Liechtenstein', 'li'),
(125, 'Lithuania', 'lt'),
(126, 'Luxembourg', 'lu'),
(127, 'Macau', 'mo'),
(128, 'Macedonia', 'mk'),
(129, 'Madagascar', 'mg'),
(130, 'Malawi', 'mw'),
(131, 'Malaysia', 'my'),
(132, 'Maldives', 'mv'),
(133, 'Mali', 'ml'),
(134, 'Malta', 'mt'),
(135, 'Marshall Islands', 'mh'),
(136, 'Martinique', 'mq'),
(137, 'Mauritania', 'mr'),
(138, 'Mauritius', 'mu'),
(139, 'Mayotte', 'yt'),
(140, 'Mexico', 'mx'),
(141, 'Micronesia', 'fm'),
(142, 'Moldova', 'md'),
(143, 'Monaco', 'mc'),
(144, 'Mongolia', 'mn'),
(145, 'Montenegro', 'me'),
(146, 'Montserrat', 'ms'),
(147, 'Morocco', 'ma'),
(148, 'Mozambique', 'mz'),
(149, 'Namibia', 'na'),
(150, 'Nauru', 'nr'),
(151, 'Nepal', 'np'),
(152, 'Netherlands', 'nl'),
(153, 'Netherlandsantilles', 'an'),
(154, 'New Caledonia', 'nc'),
(155, 'New Guinea', 'pg'),
(156, 'New Zealand', 'nz'),
(157, 'Nicaragua', 'ni'),
(158, 'Niger', 'ne'),
(159, 'Nigeria', 'ng'),
(160, 'Niue', 'nu'),
(161, 'Norfolk Island', 'nf'),
(162, 'North Korea', 'kp'),
(163, 'Northern Mariana Islands', 'mp'),
(164, 'Norway', 'no'),
(165, 'Oman', 'om'),
(166, 'Pakistan', 'pk'),
(167, 'Palau', 'pw'),
(168, 'Palestine', 'ps'),
(169, 'Panama', 'pa'),
(170, 'Paraguay', 'py'),
(171, 'Peru', 'pe'),
(172, 'Philippines', 'ph'),
(173, 'Pitcairn Islands', 'pn'),
(174, 'Poland', 'pl'),
(175, 'Portugal', 'pt'),
(176, 'Puerto Rico', 'pr'),
(177, 'Qatar', 'qa'),
(178, 'Reunion', 're'),
(179, 'Romania', 'ro'),
(180, 'Russia', 'ru'),
(181, 'Rwanda', 'rw'),
(182, 'Saint Helena', 'sh'),
(183, 'Saint Kitts and Nevis', 'kn'),
(184, 'Saint Lucia', 'lc'),
(185, 'Saint Pierre', 'pm'),
(186, 'Saint Vincent', 'vc'),
(187, 'Samoa', 'ws'),
(188, 'San Marino', 'sm'),
(189, 'Sandwich Islands', 'gs'),
(190, 'Sao Tome', 'st'),
(191, 'Saudi Arabia', 'sa'),
(192, 'Scotlandsct', 'gb'),
(193, 'Senegal', 'sn'),
(194, 'Serbia', 'cs'),
(195, 'Serbia', 'rs'),
(196, 'Seychelles', 'sc'),
(197, 'Sierra Leone', 'sl'),
(198, 'Singapore', 'sg'),
(199, 'Slovakia', 'sk'),
(200, 'Slovenia', 'si'),
(201, 'Solomon Islands', 'sb'),
(202, 'Somalia', 'so'),
(203, 'South Africa', 'za'),
(204, 'South Korea', 'kr'),
(205, 'Spain', 'es'),
(206, 'Sri Lanka', 'lk'),
(207, 'Sudan', 'sd'),
(208, 'Suriname', 'sr'),
(209, 'Swaziland', 'sz'),
(210, 'Sweden', 'se'),
(211, 'Switzerland', 'ch'),
(212, 'Syria', 'sy'),
(213, 'Taiwan', 'tw'),
(214, 'Tajikistan', 'tj'),
(215, 'Tanzania', 'tz'),
(216, 'Thailand', 'th'),
(217, 'Timorleste', 'tl'),
(218, 'Togo', 'tg'),
(219, 'Tokelau', 'tk'),
(220, 'Tonga', 'to'),
(221, 'Trinidad', 'tt'),
(222, 'Tunisia', 'tn'),
(223, 'Turkey', 'tr'),
(224, 'Turkmenistan', 'tm'),
(225, 'Tuvalu', 'tv'),
(226, 'U.A.E.', 'ae'),
(227, 'Uganda', 'ug'),
(228, 'Ukraine', 'ua'),
(229, 'United Kingdom', 'gb'),
(230, 'United States', 'us'),
(231, 'Uruguay', 'uy'),
(232, 'US Minor Islands', 'um'),
(233, 'US Virgin Islands', 'vi'),
(234, 'Uzbekistan', 'uz'),
(235, 'Vanuatu', 'vu'),
(236, 'Vatican City', 'va'),
(237, 'Venezuela', 've'),
(238, 'Vietnam', 'vn'),
(239, 'Waleswls', 'gb'),
(240, 'Wallis and Futuna', 'wf'),
(241, 'Western Sahara', 'eh'),
(242, 'Yemen', 'ye'),
(243, 'Zambia', 'zm'),
(244, 'Zimbabwe', 'zw');

-- --------------------------------------------------------

--
-- Struttura della tabella `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `likes`
--

INSERT INTO `likes` (`id`, `recipe_id`, `user_id`) VALUES
(21, 150, 20),
(22, 155, 22),
(23, 152, 22),
(24, 151, 23),
(25, 155, 23),
(26, 158, 23),
(27, 164, 23),
(28, 162, 23),
(29, 150, 23),
(30, 163, 23),
(32, 152, 19),
(35, 156, 19),
(36, 154, 19),
(43, 151, 19),
(46, 150, 19),
(48, 155, 19);

-- --------------------------------------------------------

--
-- Struttura della tabella `occasion`
--

CREATE TABLE `occasion` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `occasion`
--

INSERT INTO `occasion` (`id`, `name`) VALUES
(1, 'Everyday dishes'),
(2, 'Cook in stock'),
(3, 'Party Food'),
(4, 'On the go'),
(5, 'Comfort food'),
(6, 'Child-friendly'),
(7, 'Grillparty'),
(9, 'Christmas'),
(10, 'Halloween'),
(11, 'Easter'),
(12, 'Valentines Day'),
(13, 'Oktoberfest');

-- --------------------------------------------------------

--
-- Struttura della tabella `recipe`
--

CREATE TABLE `recipe` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `difficulty_id` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `recipetype` int(11) DEFAULT NULL,
  `kitchenland` int(11) DEFAULT NULL,
  `portions` int(11) DEFAULT NULL,
  `portionUnit` varchar(64) DEFAULT NULL,
  `preparationTime` int(6) DEFAULT NULL,
  `cookingTime` int(6) DEFAULT NULL,
  `waitingTime` int(6) DEFAULT NULL,
  `description` text,
  `public` int(11) DEFAULT '1',
  `image_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `recipe`
--

INSERT INTO `recipe` (`id`, `user_id`, `name`, `difficulty_id`, `category`, `recipetype`, `kitchenland`, `portions`, `portionUnit`, `preparationTime`, `cookingTime`, `waitingTime`, `description`, `public`, `image_id`, `created`, `updated`) VALUES
(150, 20, 'Panino salame e mozzarella', 2, 3, 4, 1, 2, 'Portion', 10, 0, 0, 'Ingredienti\n•1 panino \n•1 mozzarella \n•3 fette d prosciutto cotto\n•4 fette di salame piccante\n•1 pomodoro\n•Salsa tonnata q.b.\n------------------------------------------------------------------\nDrain and cut the mozzarella into slices. Wrap the mozzarella slices with the slices of cooked ham and roast these rolls on the plate for just 2 minutes. Spread the tuna sauce (about 2 tablespoons) on the bun. Wash and slice the tomato, adding them to the sandwich together with the slices of spicy salami and the rolls of ham and mozzarella. Finally, heat the sandwich for a few minutes.\nAccorgimenti\nLasciate sulla piastra gli involtini di mozzarella e prosciutto cotto solo per qualche minuto, in maniera da riscaldare il prosciutto senza rischiare di bruciarlo.\nIdee e varianti\nInvece della salsa tonnata potete utilizzare la salsa rosa, oppure la senape, o la maionese.', 1, 84, '2020-02-02 10:17:26', '2020-02-02 10:18:43'),
(151, 20, 'Carbonara', 3, 3, 2, 1, 2, 'Portion', 10, 15, 0, 'INGREDIENTI\n680 CALORIE PER PORZIONE\nSpaghetti 320 g\nGuanciale 150 g\nTuorli di uova medie 6\nPecorino romano 50 g\nSale fino q.b.\nPepe nero q.b.\nPer preparare gli spaghetti alla carbonara cominciate mettendo sul fuoco una pentola con l’acqua salata per cuocere la pasta. Nel frattempo eliminate la cotenna dal guanciale 1 e tagliatelo prima a fette e poi a striscioline spesse circa 1cm 2. La cotenna avanzata potrà essere riutilizzata per insaporire altre preparazioni. Versate i pezzetti in una padella antiaderente 3 e rosolate per circa 15 minuti a fiamma media, fate attenzione a non bruciarlo altrimenti rilascerà un aroma troppo forte.\n\n\nNel frattempo tuffate gli spaghetti nell’acqua bollente 4 e cuoceteli per il tempo indicato sulla confezione. Intanto versate i tuorli in una ciotola 5, aggiungete anche la maggior parte del Pecorino previsto dalla ricetta 6 la parte restante servirà per guarnire la pasta.\n\n\nInsaporite con il pepe nero 7, amalgamate tutto con una frusta a mano 8. Aggiungete un cucchiaio di acqua di cottura per diluire il composto e mescolate 9.\n\n\nIntanto il guanciale sarà giunto a cottura, spegnete il fuoco e tenetelo da parte 10. Scolate la pasta al dente direttamente nel tegame con il guanciale 11 e saltatela brevemente per insaporirla. Togliete dal fuoco e versate il composto di uova e Pecorino 12 nel tegame. Mescolate velocemente per amalgamare.\n\n\nPer renderla ben cremosa, al bisogno, potete aggiungere poca acqua di cottura della pasta 13. Servite subito gli spaghetti alla carbonara insaporendoli con il Pecorino avanzato 14 e il pepe nero macinato a piacere 15.\n\nCONSERVAZIONE\nSi consiglia di consumare subito gli spaghetti alla carbonara.\nSi sconsigliano forme di conservazione\n\nCONSIGLIO\nCome potete arricchire gli spaghetti alla carbonara? Innanzitutto rispettando i vostri gusti, in cucina è sempre importantissimo! Altrimenti eccovi delle validissime alternative. Per esempio in alternativa agli spaghetti si possono usare anche rigatoni o mezze maniche e invece del guanciale provate con della pancetta tesa utilizzando un grasso in padella, dell\'olio o del burro. Poi sostituite il Pecorino o fate a metà con del Parmigiano grattugiato. E per finire, per rendere ancora più corposa la vostra carbonara, non ci sarà bisogno di aggiungere la panna! Potete semplicemente aggiungere poca acqua di cottura della pasta oppure usare una combinazione di uova intere e tuorli: sperimentate per trovare la consistenza che più preferite!\nNoi abbiamo utilizzato gli spaghetti n.3 ma potete provare anche i n.5 e gli Spaghettoni!', 1, 85, '2020-02-02 10:20:53', '2020-02-02 10:20:53'),
(152, 20, 'Pasta al ragù alla bolognese', 4, 3, 2, 1, 4, 'Portion', 15, 120, 15, 'INGREDIENTI\n532 CALORIE PER PORZIONE\nCarne bovina macinato grosso 500 g\nCarne di suino macinata (molto grassa) 250 g\nPassata di pomodoro 250 g\nSedano 50 g\nCipolle dorate 50 g\nCarote 50 g\nVino bianco 250 g\nOlio extravergine d\'oliva 1 cucchiaio\nAcqua 3 l\nLatte intero 40 g\nSale fino q.b.\nPepe nero q.b.\nTo prepare the Bolognese sauce, start by finely chopping celery 1, peeled and peeled carrot 2 and peeled onion 3 with a knife. You will need 50 g for each ingredient.\n\n\nThen pour the oil into a saucepan and add the chopped letting go for about ten minutes over low heat while stirring occasionally 4. After the time the sauté must be wilted and the bottom of the pan dry instead. Add the coarse ground beef 5 and the ground pork 6.\n\n\nThese too will have to brown slowly for about ten minutes, stirring occasionally. Initially all the juices will come out but once dried you can blend with the white wine 7. As soon as the alcohol has evaporated and the bottom has returned, once again, dry add the tomato puree 8. Then pour only 1 of the 3 liters of water 9,\n\n\nadd a pinch of salt, mix and cook over medium-low heat for an hour. After the first hour, you can add another liter of water, mix and cook for another hour. At the end of the second hour of cooking, pour the last liter of water and continue to cook on a low flame for another hour. In this way the ragù will cook for at least 3 hours 10. At the end of cooking the result will be very dry, season with salt and pepper, turn off the heat and add the milk 11; one last mix and here is the Bolognese sauce ready for use 12!\n\nSTORAGE\nYou can prepare bolognese ragù in advance and heat it when needed.\nYou can keep it in a glass container, well covered with cling film, for a maximum of 2-3 days.\nIf you prefer you can also freeze the meat sauce.\n\nADVICE\nThe traditions related to the Bolognese sauce recipe are really many! Let\'s start with the meat.\nPork must be very fatty, therefore in jargon it is called pancetta (therefore it is not the classic cooking bacon). The bovine one, on the other hand, must be coarse-grained, so as to give the ragù a rustic consistency.\nThe amount of sauce does not have to be large, in spite of what is commonly believed. The tagliatelle will therefore be seasoned with a restricted sauce unlike the lasagna which instead needs a softer sauce.\nThe milk serves to make the ragù more full-bodied and creamy, but in ancient times a generous spoonful of lard was used which can still be used to reinforce a slightly fatty pork.', 1, 88, '2020-02-02 10:23:59', '2020-02-02 10:28:22'),
(153, 20, 'Fruit Yougurt', 1, 2, 5, 76, 1, 'Portion', 15, 0, 0, 'Ingredients\nYogurt\n9 cups (40 ounces) organic whole milk\n1 tablespoon yogurt starter or 1/2 cup yogurt with active live cultures\nFruit puree (recipe below)\nMaple syrup to taste\nFruit Puree\n2 cups ripe fruit (berries, chopped peaches or pears, etc.)\n1 teaspoon turbinado sugar\nHeat the milk gently in a large, heavy saucepan until it starts to steam. Remove the pan from the heat and let the milk cool to room temperature.\nIn a small bowl, whisk together the starter or yogurt and about a 1/4 cup of the lukewarm milk until smooth. Whisk this into the saucepan with the rest of the milk.\nTransfer the milk to a measuring cup or bowl with a spout and pour carefully into seven 7-ounce yogurt jars (make sure these are clean and dry). Arrange the jars, without their lids, in the base of the yogurt maker and cover the base with the clear plastic lid. Plug in the yogurt maker and set the timer for 7 to 8 hours, depending on how firm you like your yogurt.\nWhen the yogurt is set, screw the lids onto the jars and refrigerate them for at least 3 hours.\nOnce the yogurt is chilled, stir a few spoonfuls of the fruit puree and a bit of maple syrup (taste as you go) into each pot of yogurt.\nFruit Puree\nCombine the fruit and sugar in a small heavy saucepan and add 1/4 cup water. Bring to a boil over medium heat, cover and lower the heat until just simmering. Cook for about 5 minutes, until the fruit starts to soften and release its juices. Mash the fruit and strain it through a fine mesh sieve. Set aside to cool and then cover and refrigerate.', 0, 87, '2020-02-02 10:26:23', '2020-02-02 10:26:23'),
(154, 19, 'Pasta al pesto', 2, 2, 2, 1, 2, 'Portion', 5, 15, 0, '1/2 cup chopped onion2 1/2 tablespoons pesto2 tablespoons olive oil2 tablespoons grated Parmesan cheese1 (16 ounce) package pastasalt to tasteground black pepper to taste\nCook pasta in a large pot of boiling water until done. Drain.\nMeanwhile, heat the oil in a frying pan over medium low heat. Add pesto, onion, and salt and pepper. Cook about five minutes, or until onions are soft.\nIn a large bowl, mix pesto mixture into pasta. Stir in grated cheese. Serve.', 0, 89, '2020-02-02 10:30:26', '2020-02-02 14:08:48'),
(155, 19, 'Pizza Margherita Napoletana DOC', 5, 2, 2, 1, 1, 'Portion', 40, 2, 480, 'INGREDIENTS\n1 12-inch round of pizza dough, stretched (see recipe)\n3 tablespoons tomato sauce (see note)\n Extra-virgin olive oil\n2 ¾ ounces fresh mozzarella\n4 to 5 basil leaves, roughly torn\n\nPREPARATION\nPlace a pizza stone or tiles on the middle rack of your oven and turn heat to its highest setting. Let it heat for at least an hour.\nPut the sauce in the center of the stretched dough and use the back of a spoon to spread it evenly across the surface, stopping approximately 1/2 inch from the edges.\nDrizzle a little olive oil over the pie. Break the cheese into large pieces and place these gently on the sauce. Scatter basil leaves over the top.\nUsing a pizza peel, pick up the pie and slide it onto the heated stone or tiles in the oven. Bake until the crust is golden brown and the cheese is bubbling, approximately 4 to 8 minutes.\nTip\nIn a food processor, whiz together whole, drained canned tomatoes, a splash of olive oil and a sprinkle of salt. Keep leftover sauce refrigerated.', 1, 90, '2020-02-02 10:33:02', '2020-02-02 10:33:02'),
(156, 19, 'Melinzanosalata', 2, 2, 1, 86, 4, 'Portion', 4, 10, 0, 'INGREDIENTS\n2 large eggplants\n4 cloves garlic, peeled and minced\n1/4 cup extra virgin olive oil, plus more for serving\n3 tablespoons freshly squeezed lemon juice\n3/4 teaspoon kosher salt\n1/4 teaspoon pepper\nChopped fresh parsley leaves and olives for garnish (optional)\nINSTRUCTIONS\nPreheat the oven to 400 degrees Fahrenheit. Place the eggplants on a foil-lined baking sheet and prick a few times with a fork. Roast in the pre-heated oven for 50 to 60 minutes, turning every 15 minutes, until charred and soft.\nAllow the eggplant to rest until cool enough to handle. Peel the skin away from the flesh (if this proves difficult, simple slice the eggplant in half lengthwise and use a spoon to scoop out the meat). Roughly chop the flesh and place in a colander to drain for 15 minutes.\nTransfer the eggplant to a large bowl and gently mash with a fork. Add the garlic, olive oil, lemon juice, salt, and pepper. Stir well, taste, and add salt and pepper to taste. Chill for one hour; serve drizzled with olive oil and topped with whole olives and a sprinkling of parsley.', 1, 91, '2020-02-02 10:34:57', '2020-02-02 10:34:57'),
(157, 22, 'Chees Burger', 3, 3, 2, 230, 1, 'Portion', 5, 15, 0, 'Ingredients\n1 small onion\n\n, diced\n500g good-quality beef mince\n1 egg\n\n1 tbsp vegetable oil\n4 burger buns\nAll or any of the following to serve: sliced tomato, beetroot, horseradish sauce, mayonnaise, ketchup, handful iceberg lettuce, rocket, watercress\n\nMethod\nTip 500g beef mince into a bowl with 1 small diced onion and 1 egg, then mix.\n\nDivide the mixture into four. Lightly wet your hands. Carefully roll the mixture into balls, each about the size of a tennis ball.\n\nSet in the palm of your hand and gently squeeze down to flatten into patties about 3cm thick. Make sure all the burgers are the same thickness so that they will cook evenly.\n\nPut on a plate, cover with cling film and leave in the fridge to firm up for at least 30 mins.\n\nHeat the barbecue to medium hot (there will be white ash over the red hot coals – about 40 mins after lighting). Lightly brush 1 side of each burger with vegetable oil.\n\nPlace the burgers, oil-side down, on the barbecue. Cook for 5 mins until the meat is lightly charred. Don’t move them around or they may stick.\n\nOil the other side, then turn over using tongs. Don’t press down on the meat, as that will squeeze out the juices.\n\nCook for 5 mins more for medium. If you like your burgers pink in the middle, cook 1 min less each side. For well done, cook 1 min more.\n\nTake the burgers off the barbecue. Leave to rest on a plate so that all the juices can settle inside.\n\nSlice 4 burger buns in half. Place, cut-side down, on the barbecue rack and toast for 1 min until they are lightly charred. Place a burger inside each bun, then top with your choice of accompaniment.', 1, 92, '2020-02-02 10:39:30', '2020-02-02 10:39:30'),
(158, 22, 'Healty Salad', 1, 2, 1, 75, 1, 'Portion', 10, 0, 0, 'You\'ll love taking this Asian slaw to summer picnics or BBQs! It\'s easy to make ahead, so it\'s also one of my favorite choices for a quick weekday lunch.\nAuthor: Jeanine Donofrio\nRecipe type: Side dish\nServes: 4 as a side\nIngredients\nDressing\n¼ cup cashew or peanut butter\n2 tablespoons white miso paste\n2 tablespoons lime juice\n1 teaspoon sesame oil\n1 teaspoon grated ginger\n2 to 5 tablespoons water, or as needed\nFor the slaw:\n6 to 7 cups shredded red and/or green cabbage\nMix of peppers: I used 3 Anaheim and 3 banana peppers; or use 1 red bell and 1 poblano.\n2 scallions, chopped\n½ cup chopped cilantro, including stems\n½ cup fresh basil, Thai basil, and/or mint\n2 Thai chiles or 1 serrano pepper, diced\nSea salt\n¼ cup toasted peanuts, pepitas, and/or sesame seeds\n1 ripe peach, thinly sliced\nInstructions\nMake the dressing: In a small bowl, whisk together the cashew butter, miso paste, lime juice, sesame oil, and ginger. Whisk in enough water to create a drizzable consistency. Set aside.\nIn a dry cast-iron skillet over medium heat, char the peppers whole, rotating until the edges have a little char, about 2 minutes per side. Remove. When cool to the touch, slice in half lengthwise, remove the stem, ribbing, and seeds and slice horizontally into thin strips.\nIn a large bowl, combine the cabbage, peppers, scallions, cilantro, basil, chiles, and ¾ of the dressing. Toss until combined. Add the remaining dressing, if desired, and season to taste with a few pinches of sea salt. Serve topped with the toasted nuts, seeds, and sliced peaches.', 1, 93, '2020-02-02 10:41:35', '2020-02-02 10:41:35'),
(159, 22, 'Pastiera', 3, 2, 3, 63, 4, 'Portion', 16, 40, 20, 'Ingredients\n \nSHORTCRUST PASTRY\n300g of plain flour\n1 tbsp of sugar\n150g of unsalted butter\n1 egg\n150ml of water\nicing sugar, for dusting\nFILLING\n350g of grano cotto, or Arborio rice\n250ml of milk\n30g of unsalted butter\n1 lemon, zest only\n2 eggs\n2 egg yolks\n300g of sugar\n350g of ricotta\n40g of candied citron\n40g of candied peel, orange\n20g of orange blossom water\n1/4 tsp vanilla paste\n1\nBegin by making the pastry. Place the flour, sugar and butter in a bowl and rub together with your fingers until it has the consistency of breadcrumbs. Add the egg and mix with your hands until combined\n2\nAdd the water a little at a time, stirring well, until the mixture comes together to form a dough – you may not need to use all the water\n3\nShape the dough into a ball, wrap in cling film, and place in the fridge for at least 1 hour to chill\n4\nMeanwhile, make the filling. Place the grano cotto in a saucepan and add the milk, butter and lemon zest. Bring to the boil, then reduce to a simmer, stirring all the time until cooked – this will take about 20–25 minutes. Remove from the heat and allow to cool completely\n5\nPlace the eggs in a bowl with the sugar and whisk until the eggs turn pale\n6\nPlace the ricotta cheese in a blender and blitz until it has the consistency of whipped cream. Add it to the egg and sugar mixture, folding it in with a spatula\n7\nAdd the cooled wheat mixture, candied peel, orange flower water and the vanilla bean paste into the ricotta mixture. Stir together until combined and put to one side\n8\nPreheat the oven to 160°C/gas mark 3 and butter a 24cm x 4cm cake tin\n9\nDust a clean surface with icing sugar and evenly roll out the pastry, carefully lining the cake tin. Prick the bottom with a fork and pour in the filling, trimming the pastry from the edges of the tin to leave a neat edge. Reserve any spare pastry to decorate and form into a ball\n10\nRoll out the ball into an even layer. Using a pasta wheel, cut 8 strips long enough to be placed across the cake tin. Place 4 of them across the top of the pastiera in one direction and then place the other 4 diagonally to create lozenge shapes\n11\nTrim the edges of of the strips against the edge of the tin and place the pasteria in the oven to bake for 1 hour. After this time, cover the top with a piece of tin foil and cook for another 30 minutes. Allow it to cool completely in the tin before carefully turning it out\n12\nServe the pastiera cold with a good quality Italian espresso coffee. It makes a lovely Easter breakfast, accompaniment to afternoon tea or dessert.', 1, 94, '2020-02-02 11:20:07', '2020-02-02 11:20:07'),
(160, 22, 'Smothie', 2, 2, 6, 76, 1, 'Portion', 10, 0, 0, '1 banana\n\n1 tbsp porridge oats\n80g soft fruit (whatever you have – strawberries, blueberries, and mango all work well)\n150ml milk\n\n1 tsp honey\n\n1 tsp vanilla extract\nMethod\nPut all the ingredients in a blender and whizz for 1 min until smooth.\n\nPour the mixture into two glasses to serve.', 1, 95, '2020-02-02 11:23:47', '2020-02-02 11:23:47'),
(161, 22, 'Gulash soup', 3, 3, 2, 180, 2, 'Portion', 10, 240, 0, 'Ingredients\n1 tbsp rapeseed oil\n\n1 large onion\n\n, halved and sliced\n3 garlic cloves, sliced\n200g extra lean stewing beef\n\n, finely diced\n1 tsp caraway seeds\n2 tsp smoked paprika\n400g can chopped tomatoes\n600ml beef stock\n1 medium sweet potato\n, peeled and diced\n1 green pepper, deseeded and diced\n\nMethod\nHeat the oil in a large pan, add the onion and garlic, and fry for 5 mins until starting to colour. Stir in the beef, increase the heat and fry, stirring, to brown it.\n\nAdd the caraway and paprika, stir well, then tip in the tomatoes and stock. Cover and leave to cook gently for 30 mins.\n\nStir in the sweet potato and green pepper, cover and cook for 20 mins more or until tender. Allow to cool a little, then serve topped with the yogurt and parsley (if the soup is too hot, it will kill the beneficial bacteria in the yogurt).', 0, 96, '2020-02-02 11:39:40', '2020-02-02 11:39:40'),
(162, 23, 'Apple Pie', 2, 2, 3, 83, 8, 'Portion', 15, 40, 20, '1 recipe pastry for a 9 inch double crust pie1/2 cup unsalted butter3 tablespoons all-purpose flour1/4 cup water1/2 cup white sugar1/2 cup packed brown sugar8 Granny Smith apples - peeled, cored and sliced\nPreheat oven to 425 degrees F (220 degrees C). Melt the butter in a saucepan. Stir in flour to form a paste. Add water, white sugar and brown sugar, and bring to a boil. Reduce temperature and let simmer.\nPlace the bottom crust in your pan. Fill with apples, mounded slightly. Cover with a lattice work crust. Gently pour the sugar and butter liquid over the crust. Pour slowly so that it does not run off.\nBake 15 minutes in the preheated oven. Reduce the temperature to 350 degrees F (175 degrees C). Continue baking for 35 to 45 minutes, until apples are soft.', 1, 97, '2020-02-02 11:42:18', '2020-02-02 11:42:18'),
(163, 23, 'Polpo Affogato', 4, 5, 2, 1, 2, 'Portion', 15, 30, 10, 'If your octopus is not pre-cleaned (all frozen octopus is pre-cleaned, and if buying fresh, you can ask the fishmonger to clean it for you): Wash and clean your octopus, removing the ink sac and internal organs by making a circular cut around the beak with a paring knife. Pull away the beak (the organs will come with it).\n\nClean the octopus\nSet your octopus in a large pot with enough water to cover and bring the water to just a simmer.\n\nBoil the octopus\nEither simmer for less than 5 minutes, to 130 to 135 F (for a moist, slightly chewy texture) or simmer very gently—at just below a slight simmer (190 to 200 F). Timing varies depending on the weight of your octopus and how many you are cooking. For 2 to 3 pounds of octopus (4 servings), it will usually be between 1 to 2 hours, but the true test for doneness is: When a knife inserted where the head meets the legs slides in easily, it\'s done. \n\nCook the octopus\nOnce your octopus is tender, you can serve it in a salad (the short-cook method lends itself well to this) or mixed into a pasta or risotto. You can also grill it quickly over a high flame, to crisp up the exterior.\n\nTips\nItalian kitchen wisdom says to boil the octopus with a wine cork in the simmering liquid to keep it tender, but that is apparently nothing more than an old wive\'s tale, unsupported by science and multiple tests.\nOther nations offer their own homegrown advice: Greeks apparently traditionally gave the octopus a few good whacks against some rocks, while Spaniards might insist on using a copper pot.\nAccording to food science guru Harold McGee, the key to tender and flavorful octopus is instead blanching it for 30 seconds in boiling water and then baking it, covered, in an oven at 200 F for a few hours. It does make sense that, undiluted by cooking water, the octopus would retain more of its flavor.\nBut if you don\'t have 4 to 5 hours to spare for this method, then you can just either keep the cooking time minimal—less than 5 minutes—for a slightly chewy but still tender texture.\nIf you do have the time, use the long, slow cooking method (a gentle braise over low heat) for maximum tenderness. Slow braising in a liquid will take anywhere from about 1 to 2 hours, depending on how many pounds of octopus you are cooking.\nAnother secret to tenderness is that previously frozen octopus grows tender more quickly than fresh. It might seem counterintuitive since with many types of meat and seafood freezing can have a negative effect on both texture and flavor, but with octopus (and squid), that is not the case. But you can use either fresh or frozen (which is usually much easier to find, in any case).\nWhen buying fresh octopus, it should not have any fishy smell at all—if it does, that means it\'s already started to go bad. ', 1, 98, '2020-02-02 11:43:47', '2020-02-02 11:43:47'),
(164, 23, 'Weinachts Gans', 5, 3, 2, 83, 4, 'Portion', 20, 120, 10, 'For the Goose:\n4 1/2-pound goose (cleaned and dressed, no gizzards)\nDash salt (or to taste)\nDash pepper (or to taste)\n2 teaspoons dried thyme\n2 apples (peeled, cored, and quartered)\n1 small onion (cut lengthwise into 1/4-inch-wide strips)\n1 tablespoon salt (mixed with 1 cup water)\nFor the Gravy:\n2 tablespoons goose drippings\n2 tablespoons all-purpose flour\n1 (14-ounce) can chicken broth (or homemade chicken stock)\nOptional: salt\nOptional: pepper\nOptional: dried thyme\n\nNote: while there are multiple steps to this recipe, this dish is broken down into workable categories to help you better plan for preparation and cooking.\n\nMake the Goose\nGather the Ingredients.\n\nGrerman Christmas goose recipe weihnachtgans\nHeat oven to 350 F.\n\nWash and dry the goose, like you would a chicken or turkey. Sprinkle salt, pepper, and thyme inside the cavity and fill with apple quarters and onions. If they don’t all fit, you can cook them alongside the goose in a small, buttered dish.\n\nWash and dry the goose\nMix 1 tablespoon salt with 1 cup of water and pour that into the bottom of the roasting pan. Place the roasting rack on top. Pierce the goose skin in several places to let the fat drip out as it cooks.\n\nMix salt\nPut the goose, breast-side down, on the roasting rack and place in oven for 50 minutes. Use the salt water mixture to baste the goose several times during this period. Add more water to drip pan if necessary. Turn the goose over onto its back and cook, basting as needed, for 50 minutes.\n\nGoose\nLet the goose rest for 5 minutes as you make the gravy.\n\nGoose\nMake the Gravy\nGather the ingredients.\n\nIngredients for seasoning\nMix 2 tablespoons of the fat drippings with 2 tablespoons flour in a small saucepan and cook for 1 minute. Gradually add the chicken broth, stirring well after each addition. Add salt, pepper, and thyme, if desired, to taste. Keep warm.\n\nMix\nServe the goose and gravy with canned peach halves decorated with currant jelly (if you don\'t want to make currant jelly or can\'t find it prepared., cranberry sauce is a fine substitute), buttered Brussels sprouts, potato croquettes (or mashed potatoes), and the cooked apples and onions.', 1, 99, '2020-02-02 11:45:48', '2020-02-02 11:45:48');

-- --------------------------------------------------------

--
-- Struttura della tabella `recipetype`
--

CREATE TABLE `recipetype` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `recipetype`
--

INSERT INTO `recipetype` (`id`, `name`) VALUES
(1, 'Starter'),
(2, 'Main course'),
(3, 'Dessert'),
(4, 'Snack'),
(5, 'Breakfast'),
(6, 'Drinks'),
(7, 'Fingerfood');

-- --------------------------------------------------------

--
-- Struttura della tabella `recipe_occasion`
--

CREATE TABLE `recipe_occasion` (
  `id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `occasion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `recipe_occasion`
--

INSERT INTO `recipe_occasion` (`id`, `recipe_id`, `occasion_id`) VALUES
(260, 150, 1),
(261, 150, 2),
(262, 150, 4),
(263, 151, 1),
(264, 151, 5),
(268, 153, 1),
(269, 153, 5),
(270, 153, 4),
(271, 152, 1),
(272, 152, 2),
(273, 152, 5),
(275, 155, 3),
(276, 155, 5),
(277, 156, 2),
(278, 156, 1),
(279, 156, 5),
(280, 157, 3),
(281, 157, 4),
(282, 157, 5),
(283, 157, 7),
(284, 158, 1),
(285, 159, 5),
(286, 160, 3),
(287, 160, 6),
(288, 161, 2),
(289, 161, 1),
(290, 162, 5),
(291, 162, 6),
(292, 162, 3),
(293, 163, 1),
(294, 164, 9),
(296, 154, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `nickname` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `nickname`, `password`, `role`, `created`, `updated`) VALUES
(19, 'Luca', 'Fiorentino', 'lucafiorentino@gmail.com', 'ACH1LLEAS', '$2b$10$M3EG1d/5PyHEfvjM7A.OduKNLWyYYEPuB.LoH/iF/fY1r6hNAfi9W', 1, '2020-02-02 10:09:00', '2020-02-02 10:10:11'),
(20, 'Niklas', 'Berend', 'niklasberend@gmail.com', 'NickyDanger', '$2b$10$rat6kkXdznMTiPsN3PguEun8GVkaScMAz3UK4Ih.Q7VglkVScIomK', 1, '2020-02-02 10:10:53', '2020-02-02 10:11:36'),
(22, 'Ellie', 'Koutani', 'elliekut@gmail.com', 'elliekut', '$2b$10$3X/m3HCvVGhiyTUh25Ka/ONXmIorI3pzpMIEEcuwOOJgqkJ5W20yO', NULL, '2020-02-02 10:37:08', '2020-02-02 10:37:08'),
(23, 'Giorgio', 'Mancusi', 'mancusi@gmail.com', 'Mancusi', '$2b$10$alcOhukzIz8uQ2YEyVI0Z.QG/SuraiQQfBXcMK7lrDQllB1PQf95.', NULL, '2020-02-02 11:40:10', '2020-02-02 11:40:10');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `difficulty`
--
ALTER TABLE `difficulty`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `kitchenland`
--
ALTER TABLE `kitchenland`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `occasion`
--
ALTER TABLE `occasion`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `recipetype`
--
ALTER TABLE `recipetype`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `recipe_occasion`
--
ALTER TABLE `recipe_occasion`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `difficulty`
--
ALTER TABLE `difficulty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT per la tabella `kitchenland`
--
ALTER TABLE `kitchenland`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT per la tabella `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT per la tabella `occasion`
--
ALTER TABLE `occasion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT per la tabella `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT per la tabella `recipetype`
--
ALTER TABLE `recipetype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `recipe_occasion`
--
ALTER TABLE `recipe_occasion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;

--
-- AUTO_INCREMENT per la tabella `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
