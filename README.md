# MyRecipe-app

****************************************
*                                      *
*             MyRecipe                 *
*        By: Luca Fiorentino           *
*                                      *
****************************************

******************************************************    
*    Admin Zugangsdaten:                             *             
*                                                    *
*    |                 |`USER DATA:`              |  *
*    |-----------------|--------------------------|  * 
*    |E-mail:          |`niklasberend@gmail.com`  |  * 
*    |Password:        |`NiklasPassword `         |  *
*    |Role:            |`Admin: 1`                |  *  
*    |--------------------------------------------|  *                                     
*    |                 |`USER DATA:`              |  *
*    |-----------------|--------------------------|  * 
*    |E-mail:          |`lucafiorentino@gmail.com`|  * 
*    |Password:        |`lucapassword`            |  *
*    |Role:            |`Admin: 1`                |  *
*    |--------------------------------------------|  *                                     
*    |                 |`USER DATA:`              |  *
*    |-----------------|--------------------------|  * 
*    |E-mail:          |`elliekut@gmail.com`      |  * 
*    |Password:        |`koutanipassword`         |  *
*    |Role:            |`Guest: 0`                |  *
*    |--------------------------------------------|  *                                     
*    |                 |`USER DATA:`              |  *
*    |-----------------|--------------------------|  * 
*    |E-mail:          |`mancusi@gmail.com`       |  * 
*    |Password:        |`mancusipassword`         |  *
*    |Role:            |`Guest: 0`                |  *
*                                                    *
******************************************************
    
## Configuration und Initialisierung

**STEP 1)**
--------------------------------------------------------------------------------------
Aktualisiere in der "**Config**" Datei die im **pfad: *"my-recipe-app/src/config"*** liegt der port unter 
``'mysqlConfig': {
  host: 'localhost',
  port: `` dein Port ``,
  user: 'recipeapp_db',
  database: 'recipeapp_db',
  password: 'recipeapp_pw'
}``

**STEP 2**
--------------------------------------------------------------------------------------
Gehe auf *"**http://localhost:8888/phpMyAdmin/?lang=de**"* und erstelle ein neues "**Benutzerkonto**" unter den Menü *Benutzerkonten*
und füge dort die Daten aus der Config Datei hinzu:
    
    Benutzername: recipeapp_db
    Hostname: -> Lokal 
    Passwort: recipeapp_pw

unter "**Datenbank für Benutzerkonto**"
       
       1 Häcken setzen
       
*das gibt dir alle Rechte und erstellt eine Datenbank mit dem gleichen Namen wie den Benutzernamen*   
    
und auf **OK** unten rechts drücken.      

**STEP 3**
--------------------------------------------------------------------------------------
Clicke auf die Datenbank "**recipeapp_db**" und importiere den vom mir erstellten **dump**
dieser liegt unter: *"**my-recipe-app-server/misc/dump**"*
dann gehe auf phpMyAdmin und clicke auf "**importieren**" dort die Datei hinzufügen und auf "**OK**" klicken.

**STEP 4**
--------------------------------------------------------------------------------------
Vom Ordner *"**my-recipe-app-server/**"* das "**uploads**" Zip an der gleichen Stelle entpacken.

--------------------------------------------------------------------------------------
##### Ende Config

##Rezept Pflege

**STEP 1)**
--------------------------------------------------------------------------------------
Öffne die von mir erstellte Datei *"**my-recipe-app-server/doc/Rezept-fuer-die-pflege.xlsx**"*

**Beschreibung Datei**
In dieser Datei findest du aufgelistet die Infos die du Brauchst um ein Rezept zu pflegen.

**Bilder für die Pflege**
Die findest du unter: *"**my-recipe-app-server/doc/images**"*.

**STEP 2** 
--------------------------------------------------------------------------------------
Log Dich mit ein von oben aufgelistetes Account ein, oder erstelle ein neues ->
 
1) | **E-mail:** `niklasberend@gmail.com`   | **Password: `NiklasPassword`**  |

2) | **E-mail:** `lucafiorentino@gmail.com` | **Password: `lucapassword`**    |

3) | **E-mail:** `elliekut@gmail.com`       | **Password: `koutanipassword`** |

4) | **E-mail:** `mancusi@gmail.com`        | **Password: `mancusipassword`** |

Erstelle Dein **Rezept**












    


    